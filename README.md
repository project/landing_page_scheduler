## Contents Of This File


 - Introduction
 - Requirements
 - Installation
 - Configuration
 - Maintainers


## Introduction

- The Landing Page Scheduler (landing_page_scheduler) module provides a simple way
  for content managers to redirect their users to a certain page for a limited
  time. The module provides a configuration page where the page and the time
  window can be configured.


## Requirements


- This module requires no modules outside of Drupal core.


# Installation

 - Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.


## Configuration


 - A configuration form is provided and can be found in:

   `Administration » Configuration » System`

   Inside the form:

   - Select the time frame in which the redirect should occur;

   - Select the node to which the users should be redirected

   - Select other options

   - Click Save.


## Maintainers


Current maintainers:
 - Nelson Alves (nelson-alves) - https://www.drupal.org/u/nelson-alves

This project has been sponsored by:
 - NTT DATA
   NTT DATA – a part of NTT Group – is a trusted global innovator of IT and business services headquartered in Tokyo.
   NTT is one of the largest IT services provider in the world and has 140,000 professionals, operating in more than 50 countries.
   NTT DATA supports clients in their digital development through a wide range of consulting and strategic advisory services, cutting-edge technologies, applications, infrastructure, modernization of IT and BPOs.
   We contribute with vast experience in all sectors of economic activity and have extensive knowledge of the locations in which we operate.

