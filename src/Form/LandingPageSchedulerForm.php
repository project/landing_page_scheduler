<?php

namespace Drupal\landing_page_scheduler\Form;

use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Extends ConfigFormBase with the LandingPageSchedulerForm options.
 */
class LandingPageSchedulerForm extends ConfigFormBase {

  /**
   * Provides an interface for an entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Provides an interface for entity type managers.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * ReportWorkerBase constructor.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Provides an interface for an entity field manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Provides an interface for entity type managers.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(EntityFieldManagerInterface $entity_field_manager, EntityTypeManagerInterface $entity_type_manager, Connection $connection) {
    $this->entityFieldManager = $entity_field_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('entity_field.manager'),
          $container->get('entity_type.manager'),
          $container->get('database')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'landing_page_scheduler_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['landing_page_scheduler.settings'];
  }


  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Get current configuration.
    $config = $this->config('landing_page_scheduler.settings');


    // Load previously saved config.
    if (!is_null($config->get('landing_page'))) {
      $default_node = $this->entityTypeManager->getStorage('node')->load($config->get('landing_page'));
    }

    // Build form.
    $form['field_set_1'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Scheduler'),
    ];

    $form['field_set_1']['activate'] = [
      '#type' => 'checkbox',
      '#description' => $this->t('Check this box to activate a redirect to a specific on the below dates.'),
      '#title' => $this->t('Activate redirect?'),
      '#default_value' => $config->get('activate') ?? '',
    ];

    $form['field_set_1']['start_redirect'] = [
      '#type' => 'datetime',
      '#description' => $this->t('The date and time at which the redirect will be in effect.'),
      '#default_value' => $config->get('start_redirect') ? new DrupalDateTime( $config->get('start_redirect') ) : '',
      '#title' => $this->t('Activate redirect on'),
    ];

    $form['field_set_1']['stop_redirect'] = [
      '#type' => 'datetime',
      '#description' => $this->t('The date and time at which the redirect will stop.'),
      '#default_value' => $config->get('stop_redirect') ? new DrupalDateTime( $config->get('stop_redirect') ) : '',
      '#title' => $this->t('Stop redirect on'),
    ];

    $form['field_set_1']['landing_page'] = [
      '#type' => 'entity_autocomplete',
      '#description' => $this->t('The node to be selected as landing page.'),
      '#default_value' => $default_node ?? '',
      '#title' => 'Landing Page',
      '#target_type' => 'node',
    ];

    $form['field_set_1']['first_access'] = [
      '#type' => 'checkbox',
      '#description' => $this->t('When this option is selected the landing page will only be displayed on the first time the user logs in.'),
      '#title' => $this->t('Only on first access?'),
      '#default_value' => $config->get('first_access') ?? '',
      '#return_Value' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save configuration'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if(!$form_state->getValue('activate')){
      return;
    }

    if ($form_state->getValue('start_redirect') === NULL) {

      $form_state->setErrorByName('start_redirect', $this->t('Please select a date for the beginning of the redirect.'));
    }
    if ($form_state->getValue('stop_redirect') === NULL) {
      $form_state->setErrorByName('stop_redirect', $this->t('Please select a date for the end of the redirect.'));
    }
    if ($form_state->getValue('landing_page') === NULL) {
      $form_state->setErrorByName('landing_page', $this->t('Please select a node to redirect on login.'));
    }

    if ($form_state->getValue('start_redirect') !== NULL && $form_state->getValue('stop_redirect') !== NULL) {
      if ($form_state->getValue('start_redirect')->getTimestamp() >= $form_state->getValue('stop_redirect')->getTimestamp()) {
        $form_state->setErrorByName('stop_redirect', $this->t('The date for stopping the redirect should be after the start of the redirect.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $start_date_config = $form_state->getValue('start_redirect') ? $form_state->getValue('start_redirect')->__toString() : '';
    $stop_date_config = $form_state->getValue('stop_redirect') ? $form_state->getValue('stop_redirect')->__toString() : '';

    $this->configFactory->getEditable('landing_page_scheduler.settings')
    ->set('activate', $form_state->getValue('activate'))
    ->set('start_redirect', $start_date_config)
    ->set('stop_redirect', $stop_date_config)
    ->set('landing_page', $form_state->getValue('landing_page'))
    ->set('first_access', $form_state->getValue('first_access'))
    ->save();


    // Save field value in database.
    $this->connection->update('user__field_redirect_to_landing_page')->fields([
      'field_redirect_to_landing_page_value' => TRUE,
    ])->execute();

    parent::submitForm($form, $form_state);
  }

}
